# Generated by Django 2.1.1 on 2018-10-03 08:43

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JadwalPribadi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=30)),
                ('tempat_kegiatan', models.CharField(max_length=17)),
                ('waktu_kegiatan', models.DateField()),
                ('kategori_kegiatan', models.CharField(max_length=6)),
            ],
        ),
    ]
