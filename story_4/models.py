from django.db import models
from datetime import date

class JadwalPribadi(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    tempat_kegiatan = models.CharField(max_length=17)
    waktu_kegiatan = models.DateTimeField()
    kategori_kegiatan = models.CharField(max_length=25)


    def __str__(self):
        return self.nama_kegiatan