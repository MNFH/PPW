from django.conf.urls import url

from .views import formKegiatan, daftarKegiatan, hapusKegiatan

urlpatterns = [ 
    url(r'^create/', formKegiatan, name='form-kegiatan'),
    url(r'^daftar/', daftarKegiatan, name='daftar-kegiatan'),
    url(r'^daftar-hapus/',hapusKegiatan, name='hapus-kegiatan')
]