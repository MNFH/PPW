from django import forms
from .models import JadwalPribadi

class JadwalPribadiForm(forms.ModelForm):

    waktu_kegiatan = forms.DateTimeField(
        widget=forms.DateTimeInput(
            attrs={'class': 'form-control', 'type': 'datetime-local'}
        ),
        label='Waktu Kegiatan',
        input_formats=['%Y-%m-%dT%H:%M'],
        error_messages={'invalid': 'Masukkan waktu yang benar.'}
    )

    class Meta:
        model = JadwalPribadi
        fields = ['nama_kegiatan', 'tempat_kegiatan','waktu_kegiatan', 'kategori_kegiatan',]
        labels = {
            'nama_kegiatan': 'Nama Kegiatan',
            'tempat_kegiatan':'Tempat Kegiatan',
            'kategori_kegiatan' : 'Kategori Kegiatan',
        }
        error_messages = {
            'nama_kegiatan': {'max_length': 'Maksimal karakter hanya 30!'},
            'tempat_kegiatan': {'max_length': 'Maksimal karakter hanya 17!'},
            'kategori_kegiatan' : {'max_length': 'maksimal karakter 10!'},
        }
        widgets = {
            'nama_kegiatan': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi nama'}),
            'tempat_kegiatan': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi tempat'}),
            'kategori_kegiatan': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'isi kategori'}),
        }
