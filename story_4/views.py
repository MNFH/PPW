from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from .models import JadwalPribadi
from .forms import JadwalPribadiForm

def about(request):
    return render(request, 'about.html')

def guest_book(request):
    return render(request, 'guest-book.html')

def education(request):
    return render(request, 'education.html')

def formKegiatan(request):
    if request.method == 'POST':
        form = JadwalPribadiForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/schedule/daftar')
    else:
        form = JadwalPribadiForm()
    return render (request, 'forms.html', {'form' : form})

def daftarKegiatan(request):
    all_kegiatan = JadwalPribadi.objects.all()
    return render(request, 'daftar-kegiatan.html', {'all_kegiatan': all_kegiatan})
    
def hapusKegiatan(request):
    JadwalPribadi.objects.all().delete()
    return HttpResponseRedirect('/schedule/daftar')